terraform {
  backend "http" {
  }
}

provider "aws" {
  region = "us-east-1"
}

data "aws_security_group" "ec2_sg" {
  name = "ec2-sg"
}

data "aws_ami" "prebaked_ami" {
  owners      = ["self"]
  most_recent = true
  filter {
    name   = "name"
    values = ["java-with-db-access-*"]
  }
}

data "aws_lb_target_group" "spring_app_tg" {
  name = "spring-app-tg"
}

# Could have put EC2 instances into a priv backend subnet, but meh, accessing them for debugging was too much of a hassle at the moment
data "aws_subnet" "pub" {
  filter {
    name   = "tag:Name"
    values = ["spring-app-network-public-*"]
  }
}

resource "aws_key_pair" "spring_app" {
  key_name   = "spring_app"
  public_key = var.pub_key
}

module "asg" {
  source = "terraform-aws-modules/autoscaling/aws"
  # Autoscaling group
  name                = "spring-app"
  min_size            = 2
  max_size            = 4
  desired_capacity    = 4
  health_check_type   = "EC2"
  vpc_zone_identifier = [data.aws_subnet.pub.id]
  target_group_arns   = [data.aws_lb_target_group.spring_app_tg.arn]
  instance_refresh = {
    strategy = "Rolling"
    preferences = {
      min_healthy_percentage = 50
    }
  }
  # Launch template
  lt_name                = "spring-app-lt"
  update_default_version = true
  use_lt                 = true
  create_lt              = true
  image_id               = data.aws_ami.prebaked_ami.id
  instance_type          = "t2.micro"
  security_groups        = [data.aws_security_group.ec2_sg.id]
  key_name               = resource.aws_key_pair.spring_app.key_name
}
