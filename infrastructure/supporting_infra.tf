provider "aws" {
  region = "us-east-1"
}

# S3 buckets for Java jar
resource "aws_s3_bucket" "jar" {
  bucket = "jar-repo-for-siim"
  acl    = "private"
}

# Networking for both webapp and db
data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  # About the interaction between subnets and azs: https://github.com/terraform-aws-modules/terraform-aws-vpc/issues/358
  source               = "terraform-aws-modules/vpc/aws"
  name                 = "spring-app-network"
  cidr                 = "10.0.0.0/16"
  azs                  = data.aws_availability_zones.available.names
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true
  public_subnets       = ["10.0.4.0/24"]
  # https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_VPC.WorkingWithRDSInstanceinaVPC.html#USER_VPC.Subnets
  # https://stackoverflow.com/questions/49862422/creating-db-instance-using-terraform
  # Each DB subnet group should have subnets in at least two Availability Zones in a given AWS Region. 
  database_subnets                   = ["10.0.5.0/24", "10.0.6.0/24"]
  database_subnet_group_name         = "db"
  create_database_subnet_route_table = true
  create_database_nat_gateway_route  = true
}

resource "aws_security_group" "ec2_sg" {
  vpc_id = module.vpc.vpc_id
  name   = "ec2-sg"
  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"
    # https://www.reddit.com/r/aws/comments/7yhs62/how_to_only_allow_traffic_from_network_load/
    # https://stackoverflow.com/a/51509503
    # Network Load Balancers are completely different. From the point of view of your instances, they are completely invisible.
    # [...] so your backend sees the connection originator as whatever called the NLB. Therefore your security groups on the backend must be open to wherever your clients are.
    # Client IP addresses is whatever your client IPs are. If they are on the open internet, 0.0.0.0/0 it is. 
    # Adding the NLB private IP address, as I saw in other responses, accomplishes nothing. Traffic is not coming from there, as far as the instances are concerned.
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Allow traffic to DB
  egress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.0.5.0/24", "10.0.6.0/24"]
  }
  # For updating
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "db_sg" {
  vpc_id = module.vpc.vpc_id
  name   = "db-sg"
  # Allow traffic from EC2 instances
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.0.4.0/24"]
  }
  # For updating
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

module "nlb" {
  source             = "terraform-aws-modules/alb/aws"
  name               = "web-nlb"
  load_balancer_type = "network"
  vpc_id             = module.vpc.vpc_id
  subnets            = module.vpc.public_subnets
  target_groups = [
    {
      name             = "spring-app-tg"
      backend_protocol = "TCP"
      backend_port     = 8080
      target_type      = "instance"
    }
  ]
  # the listeners use target_group_index = 0 to indicate that they apply to this one specific TG. 
  # If you have more TGs, you would use target_group_index = 1, 2, 3 ... to specify which listener applies to each TG.
  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "TCP"
      target_group_index = 0
    }
  ]
}

# RDS instance
resource "aws_db_instance" "spring_rds" {
  identifier        = "spring-rds"
  name              = "springapp"
  instance_class    = "db.t3.micro"
  allocated_storage = 5
  engine            = "mysql"
  username          = "spring_app"
  port              = 3306
  password          = var.db_password
  # A DB subnet group allows you to specify a particular VPC when creating DB instances using the CLI or API; 
  db_subnet_group_name   = module.vpc.database_subnet_group_name
  vpc_security_group_ids = [aws_security_group.db_sg.id]
  skip_final_snapshot    = true
}