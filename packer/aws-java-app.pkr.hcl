packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "git_sha" {
  type = string
  default = ""
}

source "amazon-ebs" "amzn2_java" {
  ami_name      = "java-with-db-access-${var.git_sha}"
  instance_type = "t2.micro"
  region        = "us-east-1"
  source_ami_filter {
    filters = {
      name                = "amzn2-ami-hvm-2.0.*-x86_64-gp2"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["amazon"]
  }
  ssh_username = "ec2-user"
  temporary_iam_instance_profile_policy_document {
    Statement {
        Action   = ["s3:Get*", "s3:List*"]
        Effect   = "Allow"
        Resource = ["*"]
    }
    Version = "2012-10-17"
  }
}

build {
  sources = [
    "source.amazon-ebs.amzn2_java"
  ]
  provisioner "ansible" {
    playbook_file = "./provision.yaml"
    user = "ec2-user"
    # use_proxy forces a localhost proxy, but the container does not have openssh, openssh-clients, openssh-server packages installed. To keep image size down, opted for use_proxy=false 
    use_proxy = false
  }
}
